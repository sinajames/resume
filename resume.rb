require 'sinatra'

set :bind, '0.0.0.0'
set :port, 80

get '/' do
  send_file './resume.pdf'
end
