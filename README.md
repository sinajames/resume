# Building your resume with Docker Containers, LaTeX, Ruby, and Sinatra
### Requirements: Docker - Full Internet Access, Ruby - Version 2.2.2


**Commands to create a new pdf + sinatra frontend container from this repo**
  - clone the base repo for your changes
* `git clone https://gitlab.com/sinajames/resume.git`
  - change the resume specific files for your resume, make note of the stringent formatting of yaml for successful recreation
*  `located at the current working directory in folder 'data'`
    Here -> [data folder] (https://gitlab.com/sinajames/resume/tree/master/data)
  - run the resume creator after changes to the resume have been made
* `ruby create-resume.rb`
  - convert the created .tex file to a PDF
* `docker run --rm -t -v ${PWD}:/source schickling/latex pdflatex resume.tex`
  - build the docker container which will hold the new resume.pdf file and host it with sinatra
* `docker build -t 'REPLACE-WITH-PROJECT-NAME'/resume .`
  - run the new container on port 80 local to the container-host
* `docker run --rm -p 80:80 'REPLACE-WITH-PROJECT-NAME'/resume`


[Example Resume CI/CD Pipeline] (https://gitlab.com/sinajames/resume/pipelines)

[Want to pull the latest built container from my registry?](https://gitlab.com/sinajames/resume/container_registry)

www.sinajames.com is run on a load balanced kubernetes cluster on 3 compute nodes and 3 pods each hosted on Google Cloud
