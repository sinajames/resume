#!/usr/bin/env ruby

require 'yaml'

Dir[File.dirname(__FILE__) + '/lib/*.rb'].each do |lib_file|
  require_relative lib_file
end

def create_resume(filename)
  page_format = YAML.load_file('./data/page_format.yaml')
  skills      = YAML.load_file('./data/skills.yaml')
  candidate   = YAML.load_file('./data/candidate.yaml')
  resume      = YAML.load_file('./data/resume.yaml')
  education   = YAML.load_file('./data/education.yaml')

  File.open(filename, 'w+') do |w|
    a = DocumentSpecs.new(page_format)
    w.write(a.generate_document_specs)

    b = ThemeProperties.new(page_format)
    w.write(b.generate_theme_properties)

    c = PageFormat.new(page_format)
    w.write(c.generate_page_format)

    d = CandidateProfile.new(candidate)
    w.write(d.generate_candidate_profile)

    e = CandidateSkills.new(skills)
    w.write(e.generate_candidate_skills)

    f = CandidateExperience.new(resume)
    w.write(f.generate_experience_section)

    g = CandidateEducation.new(education)
    w.write(g.generate_education_section)
  end
end

create_resume('resume.tex')
