class CandidateProfile
  attr_accessor :first_name, :last_name, :profession, :email, :homepage, :address, :cell, :zip, :extra_info

  def initialize(candidate_profile)
    @first_name = candidate_profile['first_name']
    @last_name  = candidate_profile['last_name']
    @profession = candidate_profile['title']
    @email      = candidate_profile['email']
    @address    = candidate_profile['address']
    @cell       = candidate_profile['cell']
    @homepage   = candidate_profile['homepage']
    @zip        = candidate_profile['zip']
    @extra_info = candidate_profile['extra_info']
  end

  def generate_name
    "\\firstname{#{first_name}}"
  end

  def generate_last_name
    "\\familyname{#{last_name}}"
  end

  def generate_profession
    "\\title{#{profession}}"
  end

  def generate_email
    "\\email{#{email}}"
  end

  def generate_homepage
    "\\homepage{#{homepage}}"
  end

  def generate_address
    "\\address{#{address}}{#{zip}}"
  end

  def generate_extra_info
    "\\extrainfo{#{extra_info}}"
  end

  def generate_cell
    "\\mobile{#{cell}}"
  end

  def begin_document
    "\\begin{document}\n\\maketitle"
  end

  def validate_candidate_profile
    instance_variables.map do |x|
      if instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_candidate_profile
    <<eot
%--- Candidate Details ---
#{generate_name}
#{generate_last_name}
#{generate_profession}
#{generate_address}
#{generate_cell}
#{generate_email}
#{generate_homepage}
#{generate_extra_info}
%--- ---

#{begin_document}

eot
  end
end
