class CandidateSkills
  attr_accessor :section, :header, :skills

  def initialize(candidate_skills)
    @section = candidate_skills['skills']
    @header  = candidate_skills['skills_section_header']
    @skills  = ""
  end

  def generate_header
    "\\section{#{header}}"
  end

  def aggregate_candidate_skills
    section.map do |x|
      skills << "\n\\cvitem{#{x['section']}}{#{x['skills']*", "}}"
    end
  end

  def generate_candidate_skills
    aggregate_candidate_skills
    <<eot
%--- Skills Section ---
#{generate_header}
#{skills}
%--- ---

eot
  end
end
