class ThemeProperties
  attr_accessor :cvstyle, :color, :package

  def initialize(theme_properties)
    @cvstyle = theme_properties['cvstyle']
    @color   = theme_properties['cvcolor']
    @package = theme_properties['package']
  end

  def generate_style
    "\\moderncvstyle{#{cvstyle}}"
  end

  def generate_color
    "\\moderncvcolor{#{color}}"
  end

  def generate_package
    "\\usepackage{#{package}}"
  end

  def validate_theme_properties
    instance_variables.map do |x|
      if instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_theme_properties
    <<eot
%--- Theme Properties ---
#{generate_style}
#{generate_color}
#{generate_package}
%--- ---

eot
  end
end
