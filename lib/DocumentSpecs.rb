class DocumentSpecs
  attr_accessor :fontsize, :papersize, :font, :theme

  def initialize(document_specs)
    @fontsize  = document_specs['page_format']['fontsize']
    @papersize = document_specs['page_format']['papersize']
    @font      = document_specs['page_format']['font']
    @theme     = document_specs['page_format']['theme']
  end

  def validate_document_specs
    instance_variables.map do |x|
      if instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_document_specs
    <<eot
%--- Document Specs ---
\\documentclass[#{fontsize},#{papersize},#{font}]{#{theme}}
%--- ---

eot
  end
end
