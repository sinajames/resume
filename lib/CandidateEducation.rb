class CandidateEducation
  attr_accessor :education_section, :education_header, :education_items

  def initialize(candidate_education)
    @education_section = candidate_education['education_section']
    @education_header  = candidate_education['education_section_header']
    @education_items   = ""
  end

  def generate_header
    "\\section{#{education_header}}"
  end

  def aggregate_education_section
    education_section.map do |x|
      education_items << "\n\\cventry{#{x['timeframe']}}{#{x['organization']}}{#{x['location']}}{#{x['major_and_gpa']}}{}{#{x['extra_info']}}"
    end
  end

  def generate_education_section
    aggregate_education_section
      <<eot
%--- Education Section ---
#{generate_header}
#{education_items}
%--- ---

\\end{document}
eot
  end
end
