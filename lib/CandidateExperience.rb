class CandidateExperience
  attr_accessor :experience_section_header, :job, :items

  def initialize(candidate_experience)
    @experience_section_header = candidate_experience['experience_section_header']
    @job                       = candidate_experience['experience']
    @items                     = ""
  end

  def generate_header
    "\\section{#{experience_section_header}}"
  end

  def validate_candidate_experience
    instance_variables.map do |x|
      if instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def aggregate_experience_section
    job.map do |x|
      items << "\n%--- #{x['company']} ---"
      items << "\n\\cventry{#{x['timeframe']}}{#{x['position']}}{#{x['company']}}{#{x['location']}}{}{#{x['company_description']}\n"
      items << "\\begin{itemize}\n"
      x['experience'].map do |t|
        items << "   \\item #{t['proficiency']}\n"
        items << "   \\begin{itemize}\n"
        t['examples'].map do |b|
          items << "     \\item #{b}\n"
        end
        items << "   \\end{itemize}\n"
      end
      items << " \\end{itemize}\n}\n"
      items << "%--- ---\n"
    end
  end

  def generate_experience_section
    aggregate_experience_section
      <<eot
%--- Experience Section ---
#{generate_header}
#{items}

eot
  end
end
