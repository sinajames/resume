class PageFormat
  attr_accessor :type, :input, :size, :shape, :width

  def initialize(page_format)
    @type  = page_format['package_format']['encoding']
    @input = page_format['package_format']['type']
    @size  = page_format['package_size']['scale']
    @shape = page_format['package_size']['type']
    @width = page_format['column_width']
  end

  def generate_encoding
    "\\usepackage[#{type}]{#{input}}"
  end

  def generate_geometry
    "\\usepackage[scale=#{size}]{#{shape}}"
  end

  def generate_hintscolumn
    "\\setlength{\\hintscolumnwidth}{#{width}}"
  end

  def validate_page_format
    instance_variables.map do |x|
      if instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_page_format
    <<eot
%--- Page Formatting ---
#{generate_encoding}
#{generate_geometry}
#{generate_hintscolumn}
%--- ---

eot
  end
end
